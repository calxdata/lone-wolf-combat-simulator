import json, math

class Combat(object):
    __default_rnt = {
            "0": 0.1,
            "1": 0.1,
            "2": 0.1,
            "3": 0.1,
            "4": 0.1,
            "5": 0.1,
            "6": 0.1,
            "7": 0.1,
            "8": 0.1,
            "9": 0.1,
        }

    __default_crt = {
        "e": {
            "1": {"-11": 0, "-10": 0, "-9": 0, "-8": 0, "-7": 0, "-6": 0, "-5": 0, "-4":  1, "-3":  1, "-2":  2, "-1":  2, "0":  3, "1":  4, "2":  4, "3":  5, "4":  5, "5":  6, "6":  6, "7":  7, "8":  7, "9":  8, "10":  8, "11":  9, },
            "2": {"-11": 0, "-10": 0, "-9": 0, "-8": 0, "-7": 0, "-6": 1, "-5": 1, "-4":  2, "-3":  2, "-2":  3, "-1":  3, "0":  4, "1":  5, "2":  5, "3":  6, "4":  6, "5":  7, "6":  7, "7":  8, "8":  8, "9":  9, "10":  9, "11": 10, },
            "3": {"-11": 0, "-10": 0, "-9": 0, "-8": 1, "-7": 1, "-6": 2, "-5": 2, "-4":  3, "-3":  3, "-2":  4, "-1":  4, "0":  5, "1":  6, "2":  6, "3":  7, "4":  7, "5":  8, "6":  8, "7":  9, "8":  9, "9": 10, "10": 10, "11": 11, },
            "4": {"-11": 0, "-10": 1, "-9": 1, "-8": 2, "-7": 2, "-6": 3, "-5": 3, "-4":  4, "-3":  4, "-2":  5, "-1":  5, "0":  6, "1":  7, "2":  7, "3":  8, "4":  8, "5":  9, "6":  9, "7": 10, "8": 10, "9": 11, "10": 11, "11": 12, },
            "5": {"-11": 1, "-10": 2, "-9": 2, "-8": 3, "-7": 3, "-6": 4, "-5": 4, "-4":  5, "-3":  5, "-2":  6, "-1":  6, "0":  7, "1":  8, "2":  8, "3":  9, "4":  9, "5": 10, "6": 10, "7": 11, "8": 11, "9": 12, "10": 12, "11": 14, },
            "6": {"-11": 2, "-10": 3, "-9": 3, "-8": 4, "-7": 4, "-6": 5, "-5": 5, "-4":  6, "-3":  6, "-2":  7, "-1":  7, "0":  8, "1":  9, "2":  9, "3": 10, "4": 10, "5": 11, "6": 11, "7": 12, "8": 12, "9": 14, "10": 14, "11": 16, },
            "7": {"-11": 3, "-10": 4, "-9": 4, "-8": 5, "-7": 5, "-6": 6, "-5": 6, "-4":  7, "-3":  7, "-2":  8, "-1":  8, "0":  9, "1": 10, "2": 10, "3": 11, "4": 11, "5": 12, "6": 12, "7": 14, "8": 14, "9": 16, "10": 16, "11": 18, },
            "8": {"-11": 4, "-10": 5, "-9": 5, "-8": 6, "-7": 6, "-6": 7, "-5": 7, "-4":  8, "-3":  8, "-2":  9, "-1":  9, "0": 10, "1": 11, "2": 11, "3": 12, "4": 12, "5": 14, "6": 14, "7": 16, "8": 16, "9": 18, "10": 18, "11": "K", },
            "9": {"-11": 5, "-10": 6, "-9": 6, "-8": 7, "-7": 7, "-6": 8, "-5": 8, "-4":  9, "-3":  9, "-2": 10, "-1": 10, "0": 11, "1": 12, "2": 12, "3": 14, "4": 14, "5": 16, "6": 16, "7": 18, "8": 18, "9": "K", "10": "K", "11": "K", },
            "0": {"-11": 6, "-10": 7, "-9": 7, "-8": 8, "-7": 8, "-6": 9, "-5": 9, "-4": 10, "-3": 10, "-2": 11, "-1": 11, "0": 12, "1": 14, "2": 14, "3": 16, "4": 16, "5": 18, "6": 18, "7": "K", "8": "K", "9": "K", "10": "K", "11": "K", },
        },
        "h": {
            "1": {"-11": "K", "-10": "K", "-9": "K", "-8": 8, "-7": 8, "-6": 6, "-5": 6, "-4": 6, "-3": 6, "-2": 5, "-1": 5, "0": 5, "1": 5, "2": 5, "3": 4, "4": 4, "5": 4, "6": 4, "7": 4, "8": 4, "9": 3, "10": 3, "11": 3},
            "2": {"-11": "K", "-10":  8, "-9":  8, "-8": 7, "-7": 7, "-6": 6, "-5": 6, "-4": 5, "-3": 5, "-2": 5, "-1": 5, "0": 4, "1": 4, "2": 4, "3": 3, "4": 3, "5": 3, "6": 3, "7": 3, "8": 3, "9": 3, "10": 3, "11": 2},
            "3": {"-11":  8, "-10":  7, "-9":  7, "-8": 6, "-7": 6, "-6": 5, "-5": 5, "-4": 5, "-3": 5, "-2": 4, "-1": 4, "0": 4, "1": 3, "2": 3, "3": 3, "4": 3, "5": 3, "6": 3, "7": 2, "8": 2, "9": 2, "10": 2, "11": 2},
            "4": {"-11":  8, "-10":  7, "-9":  7, "-8": 6, "-7": 6, "-6": 5, "-5": 5, "-4": 4, "-3": 4, "-2": 4, "-1": 4, "0": 3, "1": 3, "2": 3, "3": 2, "4": 2, "5": 2, "6": 2, "7": 2, "8": 2, "9": 2, "10": 2, "11": 2},
            "5": {"-11":  7, "-10":  6, "-9":  6, "-8": 5, "-7": 5, "-6": 4, "-5": 4, "-4": 4, "-3": 4, "-2": 3, "-1": 3, "0": 2, "1": 2, "2": 2, "3": 2, "4": 2, "5": 2, "6": 2, "7": 2, "8": 2, "9": 2, "10": 2, "11": 1},
            "6": {"-11":  6, "-10":  6, "-9":  6, "-8": 5, "-7": 5, "-6": 4, "-5": 4, "-4": 3, "-3": 3, "-2": 2, "-1": 2, "0": 2, "1": 2, "2": 2, "3": 2, "4": 2, "5": 1, "6": 1, "7": 1, "8": 1, "9": 1, "10": 1, "11": 1},
            "7": {"-11":  5, "-10":  5, "-9":  5, "-8": 4, "-7": 4, "-6": 3, "-5": 3, "-4": 2, "-3": 2, "-2": 2, "-1": 2, "0": 1, "1": 1, "2": 1, "3": 1, "4": 1, "5": 0, "6": 0, "7": 0, "8": 0, "9": 0, "10": 0, "11": 0},
            "8": {"-11":  4, "-10":  4, "-9":  4, "-8": 3, "-7": 3, "-6": 2, "-5": 2, "-4": 1, "-3": 1, "-2": 1, "-1": 1, "0": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0, "8": 0, "9": 0, "10": 0, "11": 0},
            "9": {"-11":  3, "-10":  3, "-9":  3, "-8": 2, "-7": 2, "-6": 0, "-5": 0, "-4": 0, "-3": 0, "-2": 0, "-1": 0, "0": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0, "8": 0, "9": 0, "10": 0, "11": 0},
            "0": {"-11":  0, "-10":  0, "-9":  0, "-8": 0, "-7": 0, "-6": 0, "-5": 0, "-4": 0, "-3": 0, "-2": 0, "-1": 0, "0": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0, "8": 0, "9": 0, "10": 0, "11": 0},
        },
    }

    def __init__(self, json_str):
        model = json.loads(json_str)

        self.ratio = model.get("ratio", 0)
        self.hero_ep = model.get("hero_ep", 20)
        self.enemy_ep = model.get("enemy_ep", 20)
        self.rnt = model.get("rnt", Combat.__default_rnt)
        self.crt = model.get("crt", Combat.__default_crt)
        self.print_headers = model.get("print_headers", 1)
        self.hero_ep_loss_mult_1 = model.get("hero_ep_loss_mult_1", 1.0)
        self.hero_ep_loss_mult_2 = model.get("hero_ep_loss_mult_2", 1.0)
        self.hero_ep_loss_round = model.get("hero_ep_loss_round", 1)
        self.hero_ep_loss_method = model.get("hero_ep_loss_method", "down")
        self.enemy_ep_loss_mult_1 = model.get("enemy_ep_loss_mult_1", 1.0)
        self.enemy_ep_loss_mult_2 = model.get("enemy_ep_loss_mult_2", 1.0)
        self.enemy_ep_loss_round = model.get("enemy_ep_loss_round", 1)
        self.enemy_ep_loss_method = model.get("enemy_ep_loss_method", "down")

        self.success = []
        self.failure = []
        self.avg_hero_ep = []
        self.round_cnt = []

    def get_enemy_loss(self, ratio, rn, round_num):
        if round_num < self.enemy_ep_loss_round:
            multiplier = self.enemy_ep_loss_mult_1
        else:
            multiplier = self.enemy_ep_loss_mult_2

        if self.enemy_ep_loss_method == "down":
            return math.floor(self.crt["e"][str(rn)][str(ratio)] * multiplier)
        else:
            return math.ceil(self.crt["e"][str(rn)][str(ratio)] * multiplier)

    def get_hero_loss(self, ratio, rn, round_num):
        if round_num < self.hero_ep_loss_round:
            multiplier = self.hero_ep_loss_mult_1
        else:
            multiplier = self.hero_ep_loss_mult_2

        if self.hero_ep_loss_method == "down":
            return math.floor(self.crt["h"][str(rn)][str(ratio)] * multiplier)
        else:
            return math.ceil(self.crt["h"][str(rn)][str(ratio)] * multiplier)

    def fight_recursive(self, ratio, hero_ep, enemy_ep, round_num):
        try:
            self.success[round_num]
        except IndexError:
            self.success.insert(round_num, 0)
            self.failure.insert(round_num, 0)
            self.avg_hero_ep.insert(round_num, 0)
            self.round_cnt.insert(round_num, 0)

        self.round_cnt[round_num] += len(self.rnt)

        for rand_num in self.rnt.keys():
            h_loss = self.get_hero_loss(ratio, rand_num, round_num)
            e_loss = self.get_enemy_loss(ratio, rand_num, round_num)

            if h_loss == "K" or hero_ep - h_loss < 1:
                self.failure[round_num] += self.rnt[rand_num] / pow(10, round_num)
            elif e_loss == "K" or enemy_ep - e_loss < 1:
                self.success[round_num] += self.rnt[rand_num] / pow(10, round_num)
                self.avg_hero_ep[round_num] += (hero_ep - h_loss) * self.rnt[rand_num]
            else:
                self.avg_hero_ep[round_num] += (hero_ep - h_loss) * self.rnt[rand_num] 
                self.fight_recursive(ratio, hero_ep - h_loss, enemy_ep - e_loss, round_num + 1)

    def fight(self):
        self.fight_recursive(self.ratio, self.hero_ep, self.enemy_ep, 0)

        if self.print_headers:
            print("Round\tSuccess\tFailure\tAvg EP\tSimulation Count")

        for i in range(len(self.round_cnt)):
            self.avg_hero_ep[i] /= (self.round_cnt[i] / len(self.rnt))
            print("{rnd:d}\t{succ:.{prec}f}\t{fail:.{prec}f}\t{ep:.1f}\t{cnt:d}".format(
                  rnd = i
                , succ = self.success[i]
                , fail = self.failure[i]
                , ep = self.avg_hero_ep[i]
                , cnt = self.round_cnt[i]
                , prec = len(self.round_cnt)
                )
            )
