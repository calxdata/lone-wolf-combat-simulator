#!/usr/bin/env python3

import sys, fileinput, getopt
import combat

USAGE = sys.argv[0] + " [options] [files]"

def main(argv):
    input_str = ""

    opts, args = getopt.getopt(argv, "h")

    for opt, arg in opts:
        if opt == "-h":
            print(USAGE)
            sys.exit()

    for line in fileinput.input(args):
        input_str += line

    fight = combat.Combat(input_str)

    fight.fight()

if __name__ == "__main__":
    main(sys.argv[1:])
