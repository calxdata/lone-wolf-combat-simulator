fsim - calculate the odds of success or failure of a combat from the Lone Wolf gamebooks by Joe Dever, Gary Chalk, et al.

# Synopsis

```
$ fsim [options] json-file
$ echo json-string | fsim [options] -
```

# Input

Input to the simulator is a JSON file that specifies the characteristics of the combat to simulate and the simulator options.

```
{
    "ratio": 0,
    "hero_ep": 20,
    "enemy_ep": 20,
    "print_headers": 1
}
```

# Output

The output is tab-separated variable (TSV) on stdout. Each row represents the statistics for one round of combat. The columns that includes the following fields, in order:

* Round — The number (0…n) of the round of combat.
* Success — The probability (0.0–1.0) that the combat ends successfully during this round of combat.
* Failure — The probability (0.0–1.0) that the combat ends in failure during this round of combat.
* Avg EP - The average number of Endurance Points (EP) of the hero after this round of combat, if the combat has not ended in an earlier round.
* Simulation Count — Count of the number of combats that have been simulated for this round. This is an indirect indicator for how many combats have not ended earlier.

# Analysis

See the `combat-analysis.ods` spreadsheet included in the `analysis` directory for illustrations of the kind of analysis that can be done using the output of the simulator.

To make it easier to use the spreadsheet from a *nix command line, set the `print_headers` option in the JSON input file to 0, and use `xclip` or similar.

```
$ fsim default.json | xclip
```

## Example Charts

![Cumulative Chart](analysis/cumulative-chart.png)

![Average EP Chart](analysis/avg-ep-chart.png)
